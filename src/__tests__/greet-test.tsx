/**
 * Greet should render the text hello and if a name is passed in to the component it should render hello followed by the name
 */

 import { render, screen } from "@testing-library/react";
import Greet from "../components/Greet/Greet";

 
 fit("Greet renders correctly ", () => {
   render(<Greet />);
   const textElement = screen.getByText(/Hello/i);
   expect(textElement).toBeInTheDocument();
 });

 