/**
 * Greet should render the text hello and if a name is passed in to the component it should render hello followed by the name
 */

import { render, screen } from "@testing-library/react";
import Greet from "./Greet";


test("Greet renders correctly ", () => {
  render(<Greet />);
  const textElement = screen.getByText(/Hello/i);
  expect(textElement).toBeInTheDocument();
});

test("Greet renders with a name", () => {
  render(<Greet name="Karthik" />);
  const textElement = screen.getByText("Hello Karthik")
  expect(textElement).toBeInTheDocument()
});
