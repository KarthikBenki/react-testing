import { render, screen } from "@testing-library/react";
import { Application } from "./Application";

describe("Application", () => {
  test("renders correctly", () => {
    render(<Application />);
    const nameElement = screen.getByRole("textbox",{
        name:"Name",
    });
    expect(nameElement).toBeInTheDocument();

    const bioElement = screen.getByRole('textbox',{
        name:"Bio",
    })
    expect(bioElement).toBeInTheDocument()

    const jobLocationElement = screen.getByRole("combobox");
    expect(jobLocationElement).toBeInTheDocument();

    expect(screen.getByRole("checkbox")).toBeInTheDocument();

    expect(screen.getByRole("button")).toBeInTheDocument();
  });
});
