import { render, screen } from "@testing-library/react";
import Treat from "./Treat";

describe("Treat", () => {
  test("should render Treat component", () => {
    render(<Treat />);
    const textContent = screen.getByText("Treat");
    expect(textContent).toBeInTheDocument();
  });

  test("should render Treat Modi from treat component", () => {
    render(<Treat name="Modi" />);
    const textContent = screen.getByText("Treat Modi");
    expect(textContent).toBeInTheDocument();
  });
});

describe.skip("Treat", () => {
  test("should render Treat component", () => {
    render(<Treat />);
    const textContent = screen.getByText("Treat");
    expect(textContent).toBeInTheDocument();
  });

  test("should render Treat Modi from treat component", () => {
    render(<Treat name="Modi" />);
    const textContent = screen.getByText("Treat Modi");
    expect(textContent).toBeInTheDocument();
  });
});

describe.only("Treat", () => {
  test("should render Treat component", () => {
    render(<Treat />);
    const textContent = screen.getByText("Treat");
    expect(textContent).toBeInTheDocument();
  });

  describe("Nested", () => {
    test("should render Treat Modi from treat component", () => {
      render(<Treat name="Modi" />);
      const textContent = screen.getByText("Treat Modi");
      expect(textContent).toBeInTheDocument();
    });
  });
});
