

type TreatProps = {
    name?:string
}


const Treat = (props:TreatProps) => {
  return (
    <div>Treat {props.name}</div>
  )
}

export default Treat